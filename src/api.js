export async function call(url = '', method = 'GET', body = undefined, contentType = "application/json") {
  // let token = window.token ? ('?token='+window.token) : '';
  url = '/api/' + url;
  let args = {
    method,
    header: new Headers({
      "Content-Type": contentType,
      "Accept": "application/json",
    }),
    credentials: 'same-origin',
    body: JSON.stringify(body)
  };
  let response = await fetch(
    url,
    args
  );
  if (response.ok) {
    return await response.json();
  }
}
