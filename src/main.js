import Vue from 'vue'
import App from './App'
import router from './router'
import AsyncComputed from 'vue-async-computed'
import Buefy from 'buefy'

Vue.use(AsyncComputed)
Vue.use(Buefy, {
  defaultTooltipAnimated: true
})

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
