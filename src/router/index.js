import Vue from 'vue'
import Router from 'vue-router'

import Events from '../pages/events.vue'
import Event from '../pages/event.vue'
import Guests from '../pages/guests.vue'
import Arrangement from '../pages/arrangement.vue'
import PrintCards from '../pages/print-cards.vue'
import PrintGuests from '../pages/print-guests.vue'
import PrintTables from '../pages/print-tables.vue'
import Customers from '../pages/customers.vue'
import Admin from '../pages/admin.vue'
import Company from '../pages/company.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/events',
      name: 'events',
      component: Events
    },
    {
      path: '/events/:eventId',
      name: 'event',
      component: Event
    },
    {
      path: '/events/:eventId/guests',
      name: 'guests',
      component: Guests
    },
    {
      path: '/events/:eventId/arrangement',
      name: 'arrangement',
      component: Arrangement
    },
    {
      path: '/events/:eventId/print/cards',
      name: 'print-cards',
      component: PrintCards
    },
    {
      path: '/events/:eventId/print/guests',
      name: 'print-guests',
      component: PrintGuests
    },
    {
      path: '/events/:eventId/print/tables',
      name: 'print-tables',
      component: PrintTables
    },
    {
      path: '/customers',
      name: 'customers',
      component: Customers
    },
    {
      path: '/admin',
      name: 'admin',
      component: Admin,
      beforeEnter: (to, from, next) => {
        //TODO: Verify admin
        next();
      }
    },
    {
      path: '/company',
      name: 'company',
      component: Company
    }
  ]
})
