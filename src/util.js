export function toastUpdate() {
  this.$toast.open({
    message: 'Updated',
    type: 'is-success'
  })
}

export function toastUpdateFailed() {
  this.$toast.open({
    message: 'Failed to update',
    type: 'is-danger'
  })
}

export function getUserId() {
  return window.localStorage.getItem("userId");
}
